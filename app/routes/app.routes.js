module.exports = app => {
    const sales = require("../controllers/sales.controller.js");
    const security = require("../controllers/security.controller.js");

    app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
      res.header("Access-Control-Allow-Headers", "*");
      next();
    });
  
    app.get("/sales/bymonth/:date", sales.salesByMonth);

    app.get("/sales/byweekdays/:date", sales.salesByWeekdays);

    app.get("/products/bymonth/:date", sales.productsByMonth);

    app.get("/products/byweekdays/:date", sales.productsByWeekdays);

    app.get("/products/uniquebymonth/:date", sales.uniqueProductsByMonth);

    app.get("/products/uniquebyweekdays/:date", sales.uniqueProductsByWeekdays);

    app.get("/categories/uniquebymonth/:date", sales.uniqueCategoriesByMonth);

    app.get("/categories/uniquebyweekdays/:date", sales.uniqueCategoriesByWeekdays);

    app.get("/sales/bychannel/:date", sales.salesByChannelByMonth);

    app.get("/sales/bybrand/:date", sales.salesByBrandByMonth);
    
    app.get("/sales/byproduct/:date", sales.salesByProductByMonth);
    
    app.get("/sales/bycategory/:date", sales.salesByCategoryByMonth);

    app.get("/sales/bystate/:date", sales.salesByStateByMonth);

    app.get("/sales/details/:date", sales.salesDetails);

    app.get("/security/user/:email", security.getUser);

    app.post("/security/register", security.registerUser);
  };