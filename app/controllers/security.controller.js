const Security = require("../models/security.model.js");

exports.getUser = (req, res) => {
    Security.getUser(req.params.email, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found User Data with email ${req.params.email}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving User Data with email " + req.params.email
          });
        }
      } else res.send(data);
    });
};

exports.registerUser = (req, res) => {
  console.log(req.body);
  Security.registerUser(req.body.email, req.body.name, req.body.organization, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Could not register User Data with email ${req.params.email}.`
        });
      } else {
        res.status(500).send({
          message: "Error registering User Data with email " + req.params.email
        });
      }
    } else res.send(data);
  });
};

