const Sales = require("../models/sales.model.js");

exports.salesByMonth = (req, res) => {
    Sales.getSalesByMonth(req.params.date, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Sales Data with date ${req.params.date}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Sales Data with date " + req.params.date
          });
        }
      } else res.send(data);
    });
};

exports.salesByWeekdays = (req, res) => {
    Sales.getSalesByWeekdays(req.params.date, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Sales Data with date ${req.params.dater}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Sales Data with date " + req.params.date
            });
          }
        } else res.send(data);
      });
  };

  exports.productsByMonth = (req, res) => {
    Sales.getProductsByMonth(req.params.date, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Sales Data with date ${req.params.date}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Sales Data with date " + req.params.date
          });
        }
      } else res.send(data);
    });
};  

  exports.productsByWeekdays = (req, res) => {
    Sales.getProductsByWeekdays(req.params.date, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Sales Data with date ${req.params.dater}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Sales Data with date " + req.params.date
            });
          }
        } else res.send(data);
      });
  };

  exports.uniqueProductsByMonth = (req, res) => {
    Sales.getUniqueProductsByMonth(req.params.date, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Sales Data with date ${req.params.date}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Sales Data with date " + req.params.date
          });
        }
      } else res.send(data);
    });
};  

  exports.uniqueProductsByWeekdays = (req, res) => {
    Sales.getUniqueProductsByWeekdays(req.params.date, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Sales Data with date ${req.params.dater}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Sales Data with date " + req.params.date
            });
          }
        } else res.send(data);
      });
  };  

  exports.uniqueCategoriesByMonth = (req, res) => {
    Sales.getUniqueCategoriesByMonth(req.params.date, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Sales Data with date ${req.params.date}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Sales Data with date " + req.params.date
          });
        }
      } else res.send(data);
    });
};    

  exports.uniqueCategoriesByWeekdays = (req, res) => {
    Sales.getUniqueCategoriesByWeekdays(req.params.date, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Sales Data with date ${req.params.dater}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Sales Data with date " + req.params.date
            });
          }
        } else res.send(data);
      });
  };  

  exports.salesByChannelByMonth = (req, res) => {
    Sales.getSalesByChannelByMonth(req.params.date, (err, data) => {
      if (err) {
        if (err.kind === "not_found") {
          res.status(404).send({
            message: `Not found Sales Data with date ${req.params.date}.`
          });
        } else {
          res.status(500).send({
            message: "Error retrieving Sales Data with date " + req.params.date
          });
        }
      } else res.send(data);
    });
  }

  exports.salesByBrandByMonth = (req, res) => {
      Sales.getSalesByBrandByMonth(req.params.date, (err, data) => {
        if (err) {
          if (err.kind === "not_found") {
            res.status(404).send({
              message: `Not found Sales Data with date ${req.params.date}.`
            });
          } else {
            res.status(500).send({
              message: "Error retrieving Sales Data with date " + req.params.date
            });
          }
        } else res.send(data);
      });
};

exports.salesByProductByMonth = (req, res) => {
  Sales.getSalesByProductByMonth(req.params.date, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Sales Data with date ${req.params.date}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Sales Data with date " + req.params.date
        });
      }
    } else res.send(data);
  });
};

exports.salesByCategoryByMonth = (req, res) => {
  Sales.getSalesByCategoryByMonth(req.params.date, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Sales Data with date ${req.params.date}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Sales Data with date " + req.params.date
        });
      }
    } else res.send(data);
  });
};

exports.salesByStateByMonth = (req, res) => {
  Sales.getSalesByStateByMonth(req.params.date, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Sales Data with date ${req.params.date}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Sales Data with date " + req.params.date
        });
      }
    } else res.send(data);
  });
};

exports.salesDetails = (req, res) => {
  Sales.getSalesDetails(req.params.date, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found Sales Data with date ${req.params.date}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving Sales Data with date " + req.params.date
        });
      }
    } else res.send(data);
  });
};