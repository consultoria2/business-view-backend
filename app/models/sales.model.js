const sql = require("./db.js");

const Sales = function(sales) {
    
};

Sales.getSalesByMonth = (date, result) => {
    sql.query(`select sum(fs.net_amount_us) as net_us_sales from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where year_number = year("${date}") and month_number = month("${date}")`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Sales.getSalesByWeekdays = (date, result) => {
    sql.query(`select dt.day_name, dt.day_of_week_number, sum(fs.net_amount_us) as net_us_sales from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where dt.year_number = year("${date}") and dt.month_number = month("${date}") and dt.week_of_month_number = (select t.week_of_month_number from dim_tiempo t where t.id_tiempo = cast(DATE_FORMAT("${date}", "%Y%m%d") as unsigned)) group by dt.day_name, dt.day_of_week_number order by day_of_week_number;`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Week Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Sales.getProductsByMonth = (date, result) => {
    sql.query(`select sum(fs.total_art) as net_products from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where year_number = year("${date}") and month_number = month("${date}")`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Products for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };  

  Sales.getProductsByWeekdays = (date, result) => {
    sql.query(`select dt.day_name, dt.day_of_week_number, sum(fs.total_art) as net_products from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where dt.year_number = year("${date}") and dt.month_number = month("${date}") and dt.week_of_month_number = (select t.week_of_month_number from dim_tiempo t where t.id_tiempo = cast(DATE_FORMAT("${date}", "%Y%m%d") as unsigned)) group by dt.day_name, dt.day_of_week_number order by day_of_week_number;`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Week Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };  

  Sales.getUniqueProductsByMonth = (date, result) => {
    sql.query(`select count(distinct id_articulo) as unique_products from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where year_number = year("${date}") and month_number = month("${date}")`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Unique Products for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };    

  Sales.getUniqueProductsByWeekdays = (date, result) => {
    sql.query(`select dt.day_name, dt.day_of_week_number, count(distinct id_articulo) as unique_products from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where dt.year_number = year("${date}") and dt.month_number = month("${date}") and dt.week_of_month_number = (select t.week_of_month_number from dim_tiempo t where t.id_tiempo = cast(DATE_FORMAT("${date}", "%Y%m%d") as unsigned)) group by dt.day_name, dt.day_of_week_number order by day_of_week_number;`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Week Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };    

  Sales.getUniqueCategoriesByMonth = (date, result) => {
    sql.query(`select count(distinct id_categoria) as unique_categories from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where year_number = year("${date}") and month_number = month("${date}")`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Unique Products for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };  

  Sales.getUniqueCategoriesByWeekdays = (date, result) => {
    sql.query(`select dt.day_name, dt.day_of_week_number, count(distinct id_categoria) as unique_categories from fact_sales fs inner join dim_tiempo dt on fs.id_tiempo = dt.id_tiempo where dt.year_number = year("${date}") and dt.month_number = month("${date}") and dt.week_of_month_number = (select t.week_of_month_number from dim_tiempo t where t.id_tiempo = cast(DATE_FORMAT("${date}", "%Y%m%d") as unsigned)) group by dt.day_name, dt.day_of_week_number order by day_of_week_number;`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Week Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };    


  Sales.getSalesByChannelByMonth = (date, result) => {
    sql.query(`select 
                      dca.store_name, dcl.customer_type,
                      sum(fs.net_amount_us) as net_us_sales 
              from 
                      fact_sales fs 
              inner join 
                      dim_tiempo dt on fs.id_tiempo = dt.id_tiempo 
              inner join 
                      dim_canal dca on dca.id_canal = fs.id_canal
              inner join 
                      dim_cliente dcl on dcl.id_cliente = fs.id_cliente
              where 
                      year_number = year("${date}") and month_number = month("${date}") group by dca.store_name, dcl.customer_type`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Sales.getSalesByBrandByMonth = (date, result) => {
    sql.query(`select 
                  m.brand_name, sum(fs.total_art) as total_art,
                  sum(fs.net_amount_us) as net_us_sales
              from 
                  fact_sales fs 
              inner join 
                  dim_tiempo dt on fs.id_tiempo = dt.id_tiempo 
              inner join 
                  dim_marca m on m.id_marca = fs.id_marca
              where 
                  year_number = year("${date}") and month_number = month("${date}") group by m.brand_name order by net_us_sales desc`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Sales.getSalesByProductByMonth = (date, result) => {
    sql.query(`select 
                  p.product_name, sum(fs.total_art) as total_art,
                  sum(fs.net_amount_us) as net_us_sales
              from 
                  fact_sales fs 
              inner join 
                  dim_tiempo dt on fs.id_tiempo = dt.id_tiempo 
              inner join 
                  dim_articulo p on p.id_articulo = fs.id_articulo
              where 
                  year_number = year("${date}") and month_number = month("${date}") group by p.product_name order by net_us_sales desc`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Sales.getSalesByCategoryByMonth = (date, result) => {
    sql.query(`select 
                  c.subcategory_name, sum(fs.total_art) as total_art,
                  sum(fs.net_amount_us) as net_us_sales
              from 
                  fact_sales fs 
              inner join 
                  dim_tiempo dt on fs.id_tiempo = dt.id_tiempo 
              inner join 
                  dim_categoria c on c.id_categoria = fs.id_categoria
              where 
                  year_number = year("${date}") and month_number = month("${date}") group by c.subcategory_name order by net_us_sales desc`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Sales.getSalesByStateByMonth = (date, result) => {
    sql.query(`select 
                  s.state_name, sum(fs.total_art) as total_art, round(avg(s.lat), 6) as lat, round(avg(s.lon), 6) as lon, 
                  sum(fs.net_amount_us) as net_us_sales
              from 
                  fact_sales fs 
              inner join 
                  dim_tiempo dt on fs.id_tiempo = dt.id_tiempo 
              inner join 
                  dim_geografia s on s.id_geografia = fs.id_geografia
              where 
                  year_number = year("${date}") and month_number = month("${date}") group by s.state_name order by net_us_sales desc`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Sales.getSalesDetails = (date, result) => {
    sql.query(`
      select 
          dt.day_desc,
          fs.fact_num,
          d.short_name,
          geo.state_name,
          cn.store_name,
          cl.customert_name,
          cl.customer_type,
          a.product_name,
          m.brand_name,
          ca.subcategory_name,
          round(fs.total_art) as total_art,
          round(fs.prec_vta/fs.tasa, 2) as prec_vta,
          round(fs.net_amount_us, 2) as net_amount_us,
          round(fs.descuentos_us, 2) as descuentos_us
      from 
              fact_sales fs 
      left join 
              dim_tiempo dt on fs.id_tiempo = dt.id_tiempo 
      left join 
              dim_marca m on m.id_marca = fs.id_marca
      left join
              dim_documento d on fs.id_documento = d.id_documento    
      left join
              dim_canal cn on fs.id_canal = cn.id_canal   
      left join
              dim_cliente cl on fs.id_cliente = cl.id_cliente   
      left join
              dim_articulo a on fs.id_articulo = a.id_articulo    
      left join
              dim_categoria ca on fs.id_categoria = ca.id_categoria     
      left join
              dim_geografia geo on geo.id_geografia = fs.id_geografia                           
      where 
              year_number = year("${date}") and month_number = month("${date}") order by day_desc`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("Monthly Sales for date " + date + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  module.exports = Sales;