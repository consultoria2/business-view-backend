const sql = require("./db.js");

const Security = function(security) {
    
};

Security.getUser = (email, result) => {
    sql.query(`select * from cnf_users u where trim(lower(user_email)) = trim(lower('${email}'))`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("User data for user with email " + email + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  Security.registerUser = (email, name, organization, result) => {
    sql.query(`insert into cnf_users (user_email, user_name) values (trim(lower('${email}')), trim('${name}'))`, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res) {
        console.log("User data for user with email " + email + ": ", res);
        result(null, res);
        return;
      }
  
      // not found Data with the id
      result({ kind: "not_found" }, null);
    });
  };

  module.exports = Security;