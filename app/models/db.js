const mysql = require("mysql");
const dbConfig = require("../config/db.config.js");

let config = {
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
};

let pool = mysql.createPool(config);

// Create a connection to the database
//const connection = mysql.createConnection(config);

// open the MySQL connection
/*function connectToDatabase() {
  connection.connect(error => {
    if (error) throw error;
    console.log("Successfully connected to the database.");
  });

  connection.on('error', (err) => {
    console.log('Error connecting to the database');
    console.log(err.code + ' : ' + err.message);
    setTimeout(connectToDatabase, 3000);
  });

  

  pool.on('connection', function (_conn) {
      if (_conn) {
          logger.info('Connected the database via threadId %d!!', _conn.threadId);
          _conn.query('SET SESSION auto_increment_increment=1');
      }
  });

}



connectToDatabase();*/

module.exports = pool;