const express = require("express");
const bodyParser = require("body-parser");
var fs = require('fs')
var https = require('https')

const app = express();

// parse requests of content-type: application/json
app.use(bodyParser.json());

// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to Innovart BI Lite." });
});

require("./app/routes/app.routes.js")(app);

// set port, listen for requests
// https.createServer({
//   key: fs.readFileSync('server.key'),
//   cert: fs.readFileSync('server.cert')
// }, app)
//   .listen(3000, function () {
//     console.log('Server is running on port 3000.')
//   })

  app.listen(3000, function () {
      console.log('Server is running on port 3000.')
  })